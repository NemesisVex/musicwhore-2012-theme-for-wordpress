<!DOCTYPE html>
<html>
	<head>
		<title><?php bloginfo( 'name' ); ?> <?php wp_title(); ?></title>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="alternate" type="application/rss+xml" title="Musicwhore.org RSS" href="<?php bloginfo( 'siteurl' ); ?>/feed/" />
		<link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/typography.css" type="text/css" media="screen, projection" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/layout.css" type="text/css" media="screen, projection" />
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo VIGILANTMEDIA_CDN_BASE_URI; ?>/web/js/modernizr-1.6.min.js"></script>
		<!--[if lt IE 9]><script type="text/javascript" src="<?php echo VIGILANTMEDIA_CDN_BASE_URI; ?>/web/js/html5.js"></script><![endif]-->
	<?php
		if ( is_singular() && get_option( 'thread_comments' ) )
			wp_enqueue_script( 'comment-reply' );

		wp_head();
	?>
	</head>
	<body>
		<div id="container" class="container">
			<div id="masthead" class="row">
				<div class="hidden-xs">
					<header id="masthead-title" class="col-md-12 text-center">
						<hgroup>
							<h1 id="title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
							<h2 id="subtitle"><?php bloginfo( 'description' ); ?></h2>
						</hgroup>
					</header>
				</div>

				<div class="col-md-12">
					<nav class="navbar navbar-default" role="navigation">
						<div class="container-fluid">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-nav"><?php /*_e( 'Primary Menu', 'musicwhore2014' );*/ ?>
									<a class="screen-reader-text skip-link sr-only" href="#content"><?php _e( 'Skip to content', 'musicwhore2014' ); ?></a>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<div class="visible-xs">
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand"><?php bloginfo( 'name' ); ?></a>
								</div>
							</div>
							<div class="collapse navbar-collapse" id="bs-nav">
								<?php wp_nav_menu( array( 'theme_location' => 'primary', 'fallback_cb' => 'musicwhore_page_menu' ) ); ?>
								<ul class="nav navbar-nav">
									<li>
										<ul class="nav-icon-list">
											<li><a href="https://www.facebook.com/pages/Musicwhoreorg/109288145780351" title="[Musicwhore.org Official Facebook Page]"><img src="<?php echo VIGILANTMEDIA_CDN_BASE_URI; ?>/web/images/icons/facebook.png"/></a></li>
											<li><a href="http://www.twitter.com/#!/MusicwhoreOrg/" title="[Twitter]"><img src="<?php echo VIGILANTMEDIA_CDN_BASE_URI; ?>/web/images/icons/twitter.png"/></a></li>
											<li><a href="http://www.last.fm/user/NemesisVex/" title="[Last.fm]"><img src="<?php echo VIGILANTMEDIA_CDN_BASE_URI; ?>/web/images/icons/lastfm.png"/></a></li>
											<li><a href="<?php bloginfo( 'siteurl' ); ?>/feed/" title="[Musicwhore.org Feed]"><img src="<?php echo VIGILANTMEDIA_CDN_BASE_URI; ?>/web/images/icons/feed.png"/></a></li>
										</ul>
									</li>
								</ul>
							</div>
						</div>
					</nav>
				</div>
			</div>


			<div id="content" class="row">
				<div class="col-md-9">