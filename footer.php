				</div><!-- #column-1 -->

				<?php get_sidebar('footer');?>
			</div><!-- #row -->
			
			<footer class="row">
				<ul class="pager">
					<li><?php echo previous_posts_link(); ?></li>
					<li><?php echo next_posts_link(); ?></li>
				</ul>

				<div id="footer-spacer">&nbsp;</div>
				<div id="footer-content" class="text-center">
				&#169; <?php echo gmdate('Y'); ?> Greg Bueno
				</div>
			</footer>
		</div><!-- #container -->

<?php wp_footer(); ?>

	</body>
</html>
